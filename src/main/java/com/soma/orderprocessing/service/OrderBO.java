package com.soma.orderprocessing.service;

import com.soma.orderprocessing.dto.Order;
import com.soma.orderprocessing.exception.BOException;

/**
 * @author Mahendra Prajapati
 * @since 04-08-2020
 */

public interface OrderBO {
    boolean placeOrder(Order order) throws BOException;

    boolean cancelOrder(int id) throws BOException;

    boolean deleteOrder(int id) throws BOException;
}
