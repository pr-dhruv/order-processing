package com.soma.orderprocessing.service;

import com.soma.orderprocessing.dao.OrderDAO;
import com.soma.orderprocessing.dto.Order;
import com.soma.orderprocessing.exception.BOException;
import lombok.Data;

import java.sql.SQLException;

/**
 * @author Mahendra Prajapati
 * @since 04-08-2020
 */
@Data
public class OrderBOImpl implements OrderBO {

    private OrderDAO dao;

    @Override
    public boolean placeOrder(Order order) throws BOException {
        try {
            int result = dao.create(order);
            if (result == 0)
                return false;
        } catch (SQLException exception) {
            throw new BOException(exception);
        }
        return true;
    }

    @Override
    public boolean cancelOrder(int id) throws BOException {
        try {
            Order order = dao.read(id);
            order.setStatus("cancelled");
            int result = dao.update(order);
            if (result == 0) {
                return false;
            }
        } catch (SQLException e) {
            throw new BOException(e);
        }
        return true;
    }

    @Override
    public boolean deleteOrder(int id) throws BOException {
        try {
            int result = dao.delete(id);
            if (result == 0) {
                return false;
            }
        } catch (SQLException e) {
            throw new BOException(e);
        }

        return true;
    }
}
