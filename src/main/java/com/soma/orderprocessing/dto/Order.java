package com.soma.orderprocessing.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Mahendra Prajapati
 * @since 04-08-2020
 */

@NoArgsConstructor
@Setter
@Getter
public class Order {
    private int id;
    private String status;
}
