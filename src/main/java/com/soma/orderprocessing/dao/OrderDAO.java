package com.soma.orderprocessing.dao;

import com.soma.orderprocessing.dto.Order;

import java.sql.SQLException;

/**
 * @author Mahendra Prajapati
 * @since 04-08-2020
 */
public interface OrderDAO {
    int create(Order order) throws SQLException;

    Order read(int id) throws SQLException;

    int update(Order order) throws SQLException;

    int delete(int id) throws SQLException;
}
