package com.soma.orderprocessing.dao;

import com.soma.orderprocessing.dto.Order;

import java.sql.SQLException;

/**
 * @author Mahendra Prajapati
 * @since 04-08-2020
 */
public class OrderDAOImpl implements OrderDAO {
    @Override
    public int create(Order order) throws SQLException {
        return 0;
    }

    @Override
    public Order read(int id) throws SQLException {
        return null;
    }

    @Override
    public int update(Order order) throws SQLException {
        return 0;
    }

    @Override
    public int delete(int id) throws SQLException {
        return 0;
    }
}
