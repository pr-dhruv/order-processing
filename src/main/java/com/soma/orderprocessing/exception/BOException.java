package com.soma.orderprocessing.exception;

import java.sql.SQLException;

/**
 * @author Mahendra Prajapati
 * @since 04-08-2020
 */

public class BOException extends RuntimeException {
    public BOException(SQLException exception) {
        super(exception);
    }
}
