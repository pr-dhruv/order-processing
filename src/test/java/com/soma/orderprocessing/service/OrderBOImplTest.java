package com.soma.orderprocessing.service;

import com.soma.orderprocessing.dao.OrderDAO;
import com.soma.orderprocessing.dto.Order;
import com.soma.orderprocessing.exception.BOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Mahendra Prajapati
 * @since 04-08-2020
 */
public class OrderBOImplTest {

    private static final int ORDER_ID = 123;

    @Mock
    private OrderDAO dao;

    private OrderBOImpl service;

    private Order order;

    private boolean result;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new OrderBOImpl();
        service.setDao(dao);
        order = new Order();
        result = false;
    }

    @Test
    public void placeOrder_CreateAnOrder() throws SQLException, BOException {

        // Setting the expectation
        // when(dao.create(order)).thenReturn(new Integer("1"));
        when(dao.create(any(Order.class))).thenReturn(new Integer("1"));

        result = service.placeOrder(order);

        assertTrue(result);
        verify(dao).create(order);
    }

    @Test
    public void placeOrder_OrderWillNoCreated() throws SQLException, BOException {

        when(dao.create(any(Order.class))).thenReturn(new Integer("0"));

        result = service.placeOrder(order);

        assertFalse(result);
        verify(dao).create(order);
    }

    @Test(expected = BOException.class)
    public void placeOrder_OrderWillThrowException() throws SQLException, BOException{
        when(dao.create(any(Order.class))).thenThrow(SQLException.class);
        result = service.placeOrder(order);
    }

    @Test
    public void cancelOrder_OrderWillBeCancelled() throws SQLException {
        when(dao.read(ORDER_ID)).thenReturn(order);
        when(dao.update(order)).thenReturn(1);
        assertTrue(service.cancelOrder(ORDER_ID));

        verify(dao).read(ORDER_ID);
        verify(dao).update(order);
    }

    @Test
    public void cancelOrder_OrderWillNotBeCancelled() throws SQLException {
        when(dao.read(ORDER_ID)).thenReturn(order);
        when(dao.update(order)).thenReturn(0);
        assertFalse(service.cancelOrder(ORDER_ID));

        verify(dao).read(ORDER_ID);
        verify(dao).update(order);
    }

    @Test(expected = BOException.class)
    public void cancelOrder_ExceptionWillBeThrownByRead() throws SQLException, BOException {
        when(dao.read(ORDER_ID)).thenThrow(SQLException.class);
        service.cancelOrder(ORDER_ID);
    }

    @Test(expected = BOException.class)
    public void cancelOrder_ExceptionWillBeThrownByUpdate() throws SQLException, BOException {
        when(dao.read(ORDER_ID)).thenReturn(order);
        when(dao.update(order)).thenThrow(SQLException.class);
        service.cancelOrder(ORDER_ID);
    }

    @Test
    public void deleteOrder_OrderWillbeDeletedWithGivenOrderId() throws SQLException {
        when(dao.delete(ORDER_ID)).thenReturn(new Integer("1"));
        assertTrue(service.deleteOrder(ORDER_ID));
        verify(dao).delete(ORDER_ID);
    }

    @Test
    public void deleteOrder_OrderWillNotbeDeletedWithGivenOrderId() throws SQLException {
        when(dao.delete(ORDER_ID)).thenReturn(new Integer("0"));
        assertFalse(service.deleteOrder(ORDER_ID));
        verify(dao).delete(ORDER_ID);
    }

    @Test(expected = BOException.class)
    public void deleteOrder_ExceptionWillBeThrownFromDelete() throws SQLException {
        when(dao.delete(ORDER_ID)).thenThrow(SQLException.class);
        service.deleteOrder(ORDER_ID);
    }

    @After
    public void tearDown() {
        service = null;
        order = null;
        result = false;
    }

}